var transformr = {
	defaults: {
		target: '.target',
		slider: '.slider',
		mode: 'values',
		density: '2',
		rotate_x: 0,
		rotate_y: 0,
		rotate_z: 0,
		rotate_values: [-359, -180, -90, -45, 0, 45, 90, 180, 359],
		translate_x: 0,
		translate_y: 0,
		translate_z: 0,
		perspective: 50,
		scale_x: 1,
		scale_y: 1,
		scale_z: 1,
		perspective_origin_x: 0,
		perspective_origin_y: 0,
		skew_x: 0,
		skew_y: 0,
		rotate_min: -359,
		rotate_start: 0,
		rotate_max: 359,
		translate_min: -100,
		translate_start: 0,
		translate_max: 100,
		translate_z_min: -100,
		translate_z_start: 0,
		translate_z_max: 100,
		translate_values: [-100, -50, 0, 50, 100],
		translate_z_values: [-100, -50, 0, 50, 100],
		scale_min: -10,
		scale_start: 1,
		scale_max: 10,
		scale_values: [-10, -5, 1, 5, 10],
		skew_min: -359,
		skew_start: 0,
		skew_max: 359,
		skew_values: [-359, -180, -90, -45, 0, 45, 90, 180, 359],
		perspective_min: 50,
		perspective_start: 1000,
		perspective_max: 1000,
		perspective_values: [50, 500, 1000],
		perspective_origin_min: 0,
		perspective_origin_start: 50,
		perspective_origin_max: 100,
		perspective_origin_values: [0, 25, 50, 75, 100]
	},

	options: {},

	state: {
		rotate_x: 0,
		rotate_y: 0,
		rotate_z: 0,
		translate_x: 0,
		translate_y: 0,
		translate_z: 0,
		scale_x: 1,
		scale_y: 1,
		scale_z: 1,
		skew_x: 0,
		skew_y: 0,
		perspective: 1000,
		perspective_origin_x: 50,
		perspective_origin_y: 50,
		parent_css: '',
		target_css: ''
	},

	build_matrix: function() {
		var self = this;
		return 'rotateX('+self.state.rotate_x+'deg) rotateY('+self.state.rotate_y+'deg) rotateZ('+self.state.rotate_z+'deg) translate3d('+self.state.translate_x+'%,'+self.state.translate_y+'%,'+self.state.translate_z+'px) scale3d('+self.state.scale_x+','+self.state.scale_y+','+self.state.scale_z+') skew('+self.state.skew_x+'deg,'+self.state.skew_y+'deg)';
	},

	init: function(opts) {
		var self = this;

		self.options = self.defaults;


		if(typeof opts !== 'undefined') {
			for (var attr in opts) {
				self.options[attr] = opts[attr];
			}
		}

		$('.slider.rotate').noUiSlider({
			start: self.options.rotate_start,
			range: {
				'min': self.options.rotate_min,
				'50%': self.options.rotate_start,
				'max': self.options.rotate_max
			}
		});

		$('.slider.rotate').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.rotate_values
		});

		$('.slider.translate.translate-x, .slider.translate.translate-y').noUiSlider({
			start: self.options.translate_start,
			range: {
				'min': self.options.translate_min,
				'50%': self.options.translate_start,
				'max': self.options.translate_max
			}
		});

		$('.slider.translate.translate-x, .slider.translate.translate-y').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.translate_values
		});

		$('.slider.translate.translate-z').noUiSlider({
			start: self.options.translate_z_start,
			range: {
				'min': self.options.translate_z_min,
				'50%': self.options.translate_z_start,
				'max': self.options.translate_z_max
			}
		});

		$('.slider.translate.translate-z').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.translate_z_values
		});

		$('.slider.perspective.perspective-value').noUiSlider({
			start: self.options.perspective_start,
			range: {
				'min': self.options.perspective_min,
				'max': self.options.perspective_max
			}
		});

		$('.slider.perspective.perspective-value').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.perspective_values
		});

		$('.slider.perspective.perspective-origin-x, .slider.perspective.perspective-origin-y').noUiSlider({
			start: self.options.perspective_origin_start,
			range: {
				'min': self.options.perspective_origin_min,
				'max': self.options.perspective_origin_max
			}
		});

		$('.slider.perspective.perspective-origin-x, .slider.perspective.perspective-origin-y').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.perspective_origin_values
		});

		$('.slider.scale').noUiSlider({
			start: self.options.scale_start,
			range: {
				'min': self.options.scale_min,
				'50%': self.options.scale_start,
				'max': self.options.scale_max
			}
		});

		$('.slider.scale').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.scale_values
		});

		$('.slider.skew').noUiSlider({
			start: self.options.skew_start,
			range: {
				'min': self.options.skew_min,
				'50%': self.options.skew_start,
				'max': self.options.skew_max
			}
		});

		$('.slider.skew').noUiSlider_pips({
			mode: self.options.mode,
			density: self.options.density,
			values: self.options.skew_values
		});

		$('.slider').on({
			slide: function() {
				var type = $(this).data('type');
				self.state[type] = $(this).val();

				if(type === 'perspective') {
					var target = $(self.options.target).parent();
					$(target).css({
						'perspective':self.state.perspective+'px'
					});
				} else if(type === 'perspective_origin_x' || type === 'perspective_origin_y') {
					var target = $(self.options.target).parent();
					$(target).css({
						'perspective-origin':self.state.perspective_origin_x+'% '+self.state.perspective_origin_y+'%'
					});
				} else {
					var target = $(self.options.target);
					$(target).css({
						'transform':self.build_matrix()
					});
				}

				var target_css = '.target {<br>
					transform: ' + $(self.options.target).css('transform') + ';
				<br>}';

				var parent_css = '.parent {<br>
					perspective: ' + $(self.options.target).parent().css('perspective') + ';<br>
					perspective-origin: ' + $(self.options.target).parent().css('perspective-origin') + ';
				<br>}';

				self.state.target_css = target_css;
				self.state.parent_css = parent_css;

				$('.css').html('<code>' + self.state.target_css + '<br><br>' + self.state.parent_css + '</code>');
			}
		});

		$('.btn').on('click', function(e) {
			e.preventDefault();

			if($('.css').hasClass('active')) {
				$('.css').removeClass('active');
			} else {
				$('.css').addClass('active');
			}
		});
	}
};

transformr.init();