// Include gulp
var gulp = require('gulp');

// Include plugins
var lint =       require('gulp-jshint');
var compass =    require('gulp-compass');
var uglify =     require('gulp-uglify');
var concat =     require('gulp-concat');
var minify_css = require('gulp-minify-css');
var watch =      require('gulp-watch');
var rename =     require('gulp-rename');
var reload =     require('gulp-livereload');


// Lint task
gulp.task('jshint', function() {
	gulp.src('src/assets/js/*.js')
	.pipe(lint())
	.pipe(lint.reporter('default'));
});

// SASS task
gulp.task('sass', function() {
	gulp.src('src/assets/scss/*.scss')
	.pipe(compass({
		css: 'dist/assets/css/expanded',
		sass: 'src/assets/scss',
		require: [
			'susy',
			'breakpoint'
		],
		style: 'expanded',
		environment: 'development'
	}))
	.pipe(rename('global.min.css'))
	.pipe(minify_css())
	.pipe(gulp.dest('dist/assets/css'));
});

// Vendor CSS task
gulp.task('css', function() {
	gulp.src('src/assets/css/**/*.css')
	.pipe(concat('vendor.css'))
	.pipe(gulp.dest('dist/assets/js'))
	.pipe(rename('vendor.min.css'))
	.pipe(minify_css())
	.pipe(gulp.dest('dist/assets/css'));
});

// Font icon task
gulp.task('font', function() {
	gulp.src('src/assets/font/*.*')
	.pipe(gulp.dest('dist/assets/font'));
});

// Concatenate & Minify JS task
gulp.task('scripts', function() {
	gulp.src('src/assets/js/*.js')
	.pipe(concat('global.js'))
	.pipe(gulp.dest('dist/assets/js'))
	.pipe(rename('global.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('dist/assets/js'));
});

// Copies unminified bower components
gulp.task('bower_copy', function() {
	gulp.src([
		'bower_components/html5shiv/dist/html5shiv.js',
		'bower_components/jquery/dist/jquery.js',
		'bower_components/livereload/dist/livereload.js',
		'bower_components/respond-minmax/dest/respond.min.js'
	])
	.pipe(gulp.dest('dist/assets/js/vendor'));
});

// Copies unminified vendor js
gulp.task('js_copy', function() {
	gulp.src([
		'src/assets/js/vendor/*.js'
	])
	.pipe(gulp.dest('dist/assets/js/vendor'));
});

// Copy task
gulp.task('copy', function() {
	gulp.src([
		'src/*.php',
		'src/*.htm',
		'src/*.html'
	])
	.pipe(gulp.dest('dist'));
});

// Watch task
gulp.task('watch', function() {
	gulp.watch('src/assets/js/*.js', ['scripts']);
	gulp.watch('src/assets/js/vendor/*.js', ['js_copy']);
	gulp.watch('src/assets/scss/**/*.scss', ['sass']);
	gulp.watch('src/assets/css/**/*.css', ['css']);
	gulp.watch(
		[
			'src/*.php',
			'src/*.htm',
			'src/*.html'
		],
		['copy']
	);

	// reload on file changes
	reload.listen();
	gulp.watch('dist/**').on('change', reload.changed);
});

// Default task
gulp.task('default', ['sass', 'css', 'font', 'scripts', 'copy', 'bower_copy', 'js_copy', 'watch']);